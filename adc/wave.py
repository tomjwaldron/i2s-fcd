import math

sampleRate = 96e3
freqHz = 440.0
period = int(round(96e3 / freqHz))
samples = [0] * period
fullScale24bit = 0x7fffff

for i in range(0, len(samples)):
    samples[i] = int(round(fullScale24bit * math.sin(2 * math.pi * freqHz * i / sampleRate)))

print("num samples: {}".format (len (samples)))
print("samples:")

# just print the samples
# print(samples)

# print the samples for plotting on a graph (x, y)
# for i in range(0, len(samples)):
#     print ("({},{})".format (i, samples[i]))

# print to paste into vhdl
print ("std_logic_vector(to_signed({}, 24))".format (samples[0]), end = "")
for i in range(1, len(samples)):
    print (", std_logic_vector(to_signed({}, 24))".format (samples[i]), end = "")
