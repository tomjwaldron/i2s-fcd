library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.lib.all;

entity adc is
    port (
        nrst : in std_logic;
        clk : in std_logic;
        sdata : out std_logic;
        lrclk, bclk : in std_logic
    );
end adc;

architecture rtl of adc is

    constant num_samples : natural := 218;  -- one full period of samples at 96KHz
    constant sample_width : natural := 24;  -- 24-bit samples
    constant sample_counter_width : natural := num_bits(num_samples);

    type sample_array is array (num_samples - 1 downto 0) of std_logic_vector(sample_width - 1 downto 0);

    -- sample values generated using wave.py script
    -- signal audio_samples : samp_vec (num_samples - 1 downto 0)
    --                                 (sample_width - 1 downto 0) := (
    signal audio_samples : sample_array := (
        std_logic_vector(to_signed(0, 24)), std_logic_vector(to_signed(241541, 24)), std_logic_vector(to_signed(482882, 24)), std_logic_vector(to_signed(723822, 24)), std_logic_vector(to_signed(964163, 24)), std_logic_vector(to_signed(1203703, 24)), std_logic_vector(to_signed(1442246, 24)), std_logic_vector(to_signed(1679592, 24)), std_logic_vector(to_signed(1915546, 24)), std_logic_vector(to_signed(2149911, 24)), std_logic_vector(to_signed(2382493, 24)), std_logic_vector(to_signed(2613100, 24)), std_logic_vector(to_signed(2841539, 24)), std_logic_vector(to_signed(3067623, 24)), std_logic_vector(to_signed(3291162, 24)), std_logic_vector(to_signed(3511972, 24)), std_logic_vector(to_signed(3729870, 24)), std_logic_vector(to_signed(3944675, 24)), std_logic_vector(to_signed(4156208, 24)), std_logic_vector(to_signed(4364295, 24)), std_logic_vector(to_signed(4568763, 24)), std_logic_vector(to_signed(4769442, 24)), std_logic_vector(to_signed(4966166, 24)), std_logic_vector(to_signed(5158772, 24)), std_logic_vector(to_signed(5347099, 24)), std_logic_vector(to_signed(5530993, 24)), std_logic_vector(to_signed(5710300, 24)), std_logic_vector(to_signed(5884871, 24)), std_logic_vector(to_signed(6054563, 24)), std_logic_vector(to_signed(6219234, 24)), std_logic_vector(to_signed(6378747, 24)), std_logic_vector(to_signed(6532970, 24)), std_logic_vector(to_signed(6681776, 24)), std_logic_vector(to_signed(6825042, 24)), std_logic_vector(to_signed(6962647, 24)), std_logic_vector(to_signed(7094478, 24)), std_logic_vector(to_signed(7220427, 24)), std_logic_vector(to_signed(7340387, 24)), std_logic_vector(to_signed(7454261, 24)), std_logic_vector(to_signed(7561953, 24)), std_logic_vector(to_signed(7663374, 24)), std_logic_vector(to_signed(7758440, 24)), std_logic_vector(to_signed(7847072, 24)), std_logic_vector(to_signed(7929197, 24)), std_logic_vector(to_signed(8004747, 24)), std_logic_vector(to_signed(8073659, 24)), std_logic_vector(to_signed(8135875, 24)), std_logic_vector(to_signed(8191345, 24)), std_logic_vector(to_signed(8240022, 24)), std_logic_vector(to_signed(8281865, 24)), std_logic_vector(to_signed(8316841, 24)), std_logic_vector(to_signed(8344920, 24)), std_logic_vector(to_signed(8366079, 24)), std_logic_vector(to_signed(8380300, 24)), std_logic_vector(to_signed(8387572, 24)), std_logic_vector(to_signed(8387888, 24)), std_logic_vector(to_signed(8381249, 24)), std_logic_vector(to_signed(8367659, 24)), std_logic_vector(to_signed(8347130, 24)), std_logic_vector(to_signed(8319679, 24)), std_logic_vector(to_signed(8285329, 24)), std_logic_vector(to_signed(8244109, 24)), std_logic_vector(to_signed(8196051, 24)), std_logic_vector(to_signed(8141197, 24)), std_logic_vector(to_signed(8079592, 24)), std_logic_vector(to_signed(8011287, 24)), std_logic_vector(to_signed(7936338, 24)), std_logic_vector(to_signed(7854808, 24)), std_logic_vector(to_signed(7766764, 24)), std_logic_vector(to_signed(7672280, 24)), std_logic_vector(to_signed(7571433, 24)), std_logic_vector(to_signed(7464308, 24)), std_logic_vector(to_signed(7350992, 24)), std_logic_vector(to_signed(7231581, 24)), std_logic_vector(to_signed(7106173, 24)), std_logic_vector(to_signed(6974872, 24)), std_logic_vector(to_signed(6837787, 24)), std_logic_vector(to_signed(6695031, 24)), std_logic_vector(to_signed(6546724, 24)), std_logic_vector(to_signed(6392988, 24)), std_logic_vector(to_signed(6233950, 24)), std_logic_vector(to_signed(6069743, 24)), std_logic_vector(to_signed(5900502, 24)), std_logic_vector(to_signed(5726368, 24)), std_logic_vector(to_signed(5547485, 24)), std_logic_vector(to_signed(5364002, 24)), std_logic_vector(to_signed(5176071, 24)), std_logic_vector(to_signed(4983848, 24)), std_logic_vector(to_signed(4787492, 24)), std_logic_vector(to_signed(4587165, 24)), std_logic_vector(to_signed(4383035, 24)), std_logic_vector(to_signed(4175270, 24)), std_logic_vector(to_signed(3964043, 24)), std_logic_vector(to_signed(3749528, 24)), std_logic_vector(to_signed(3531904, 24)), std_logic_vector(to_signed(3311351, 24)), std_logic_vector(to_signed(3088052, 24)), std_logic_vector(to_signed(2862193, 24)), std_logic_vector(to_signed(2633959, 24)), std_logic_vector(to_signed(2403542, 24)), std_logic_vector(to_signed(2171131, 24)), std_logic_vector(to_signed(1936920, 24)), std_logic_vector(to_signed(1701103, 24)), std_logic_vector(to_signed(1463875, 24)), std_logic_vector(to_signed(1225433, 24)), std_logic_vector(to_signed(985975, 24)), std_logic_vector(to_signed(745699, 24)), std_logic_vector(to_signed(504805, 24)), std_logic_vector(to_signed(263493, 24)), std_logic_vector(to_signed(21961, 24)), std_logic_vector(to_signed(-219588, 24)), std_logic_vector(to_signed(-460955, 24)), std_logic_vector(to_signed(-701941, 24)), std_logic_vector(to_signed(-942344, 24)), std_logic_vector(to_signed(-1181965, 24)), std_logic_vector(to_signed(-1420606, 24)), std_logic_vector(to_signed(-1658070, 24)), std_logic_vector(to_signed(-1894158, 24)), std_logic_vector(to_signed(-2128676, 24)), std_logic_vector(to_signed(-2361428, 24)), std_logic_vector(to_signed(-2592222, 24)), std_logic_vector(to_signed(-2820867, 24)), std_logic_vector(to_signed(-3047172, 24)), std_logic_vector(to_signed(-3270950, 24)), std_logic_vector(to_signed(-3492016, 24)), std_logic_vector(to_signed(-3710186, 24)), std_logic_vector(to_signed(-3925279, 24)), std_logic_vector(to_signed(-4137118, 24)), std_logic_vector(to_signed(-4345525, 24)), std_logic_vector(to_signed(-4550329, 24)), std_logic_vector(to_signed(-4751359, 24)), std_logic_vector(to_signed(-4948450, 24)), std_logic_vector(to_signed(-5141436, 24)), std_logic_vector(to_signed(-5330160, 24)), std_logic_vector(to_signed(-5514463, 24)), std_logic_vector(to_signed(-5694193, 24)), std_logic_vector(to_signed(-5869201, 24)), std_logic_vector(to_signed(-6039342, 24)), std_logic_vector(to_signed(-6204475, 24)), std_logic_vector(to_signed(-6364462, 24)), std_logic_vector(to_signed(-6519172, 24)), std_logic_vector(to_signed(-6668476, 24)), std_logic_vector(to_signed(-6812250, 24)), std_logic_vector(to_signed(-6950374, 24)), std_logic_vector(to_signed(-7082735, 24)), std_logic_vector(to_signed(-7209223, 24)), std_logic_vector(to_signed(-7329732, 24)), std_logic_vector(to_signed(-7444163, 24)), std_logic_vector(to_signed(-7552420, 24)), std_logic_vector(to_signed(-7654415, 24)), std_logic_vector(to_signed(-7750062, 24)), std_logic_vector(to_signed(-7839283, 24)), std_logic_vector(to_signed(-7922002, 24)), std_logic_vector(to_signed(-7998152, 24)), std_logic_vector(to_signed(-8067670, 24)), std_logic_vector(to_signed(-8130497, 24)), std_logic_vector(to_signed(-8186582, 24)), std_logic_vector(to_signed(-8235878, 24)), std_logic_vector(to_signed(-8278345, 24)), std_logic_vector(to_signed(-8313946, 24)), std_logic_vector(to_signed(-8342653, 24)), std_logic_vector(to_signed(-8364442, 24)), std_logic_vector(to_signed(-8379295, 24)), std_logic_vector(to_signed(-8387198, 24)), std_logic_vector(to_signed(-8388147, 24)), std_logic_vector(to_signed(-8382140, 24)), std_logic_vector(to_signed(-8369181, 24)), std_logic_vector(to_signed(-8349283, 24)), std_logic_vector(to_signed(-8322460, 24)), std_logic_vector(to_signed(-8288736, 24)), std_logic_vector(to_signed(-8248139, 24)), std_logic_vector(to_signed(-8200702, 24)), std_logic_vector(to_signed(-8146464, 24)), std_logic_vector(to_signed(-8085470, 24)), std_logic_vector(to_signed(-8017772, 24)), std_logic_vector(to_signed(-7943425, 24)), std_logic_vector(to_signed(-7862490, 24)), std_logic_vector(to_signed(-7775036, 24)), std_logic_vector(to_signed(-7681134, 24)), std_logic_vector(to_signed(-7580862, 24)), std_logic_vector(to_signed(-7474304, 24)), std_logic_vector(to_signed(-7361547, 24)), std_logic_vector(to_signed(-7242686, 24)), std_logic_vector(to_signed(-7117819, 24)), std_logic_vector(to_signed(-6987049, 24)), std_logic_vector(to_signed(-6850485, 24)), std_logic_vector(to_signed(-6708240, 24)), std_logic_vector(to_signed(-6560433, 24)), std_logic_vector(to_signed(-6407185, 24)), std_logic_vector(to_signed(-6248623, 24)), std_logic_vector(to_signed(-6084881, 24)), std_logic_vector(to_signed(-5916092, 24)), std_logic_vector(to_signed(-5742397, 24)), std_logic_vector(to_signed(-5563940, 24)), std_logic_vector(to_signed(-5380869, 24)), std_logic_vector(to_signed(-5193336, 24)), std_logic_vector(to_signed(-5001496, 24)), std_logic_vector(to_signed(-4805509, 24)), std_logic_vector(to_signed(-4605537, 24)), std_logic_vector(to_signed(-4401745, 24)), std_logic_vector(to_signed(-4194303, 24)), std_logic_vector(to_signed(-3983384, 24)), std_logic_vector(to_signed(-3769160, 24)), std_logic_vector(to_signed(-3551812, 24)), std_logic_vector(to_signed(-3331518, 24)), std_logic_vector(to_signed(-3108461, 24)), std_logic_vector(to_signed(-2882826, 24)), std_logic_vector(to_signed(-2654801, 24)), std_logic_vector(to_signed(-2424574, 24)), std_logic_vector(to_signed(-2192337, 24)), std_logic_vector(to_signed(-1958281, 24)), std_logic_vector(to_signed(-1722602, 24)), std_logic_vector(to_signed(-1485494, 24)), std_logic_vector(to_signed(-1247155, 24)), std_logic_vector(to_signed(-1007781, 24)), std_logic_vector(to_signed(-767571, 24)), std_logic_vector(to_signed(-526725, 24)), std_logic_vector(to_signed(-285442, 24))
    );

    signal counter_enable, counter_reset : std_logic;
    signal counter_value : std_logic_vector(sample_counter_width - 1 downto 0) := (others => '0');
    signal parallel_load : std_logic;

    signal sample_load : std_logic;
    signal sample_reg : std_logic_vector(sample_width - 1 downto 0) := (others => '0');

    signal lrclk_rising, lrclk_falling : std_logic;

begin

    counter_enable <= lrclk_rising;

    parallel_load <= '1' when lrclk_rising = '1' or lrclk_falling = '1' else '0';

    rising_edge_inst: entity work.edge_detector(rtl)
    port map (
        nrst => nrst, clk => clk,
        strobe => lrclk,
        detected => lrclk_rising
    );

    falling_edge_inst: entity work.edge_detector(rtl)
    generic map ( DETECT_RISING_EDGE => false )
    port map (
        nrst => nrst, clk => clk,
        strobe => lrclk,
        detected => lrclk_falling
    );

    counter_inst: entity work.counter(rtl)
    generic map(BIT_WIDTH => sample_counter_width)
    port map (
        nrst => nrst, clk => clk,
        enable => counter_enable,
        reset => counter_reset,
        count => counter_value
    );

    shift_register_inst: entity work.shift_register(rtl)
    generic map(BIT_WIDTH => sample_width)
    port map (
        clk => clk, nrst => nrst,
        shift => bclk,
        serial_out => sdata,
        parallel_load => parallel_load,
        parallel_in => sample_reg
    );

    counter_reset_register: process(nrst, clk)
    begin
        if nrst = '0' then
            counter_reset <= '0';
        elsif rising_edge(clk) then
            if unsigned(counter_value) = num_samples - 1 then
                counter_reset <= '1';
            else
                counter_reset <= '0';
            end if;
        end if;
    end process;

    sample_load_register: process(nrst, clk)
    begin
        if nrst = '0' then
            sample_load <= '0';
        elsif rising_edge(clk) then
            if counter_enable = '1' then
                sample_load <= '1';
            else
                sample_load <= '0';
            end if;
        end if;
    end process;

    sample_reg_register: process(nrst, clk)
    begin
        if nrst = '0' then
            sample_reg <= (others => '0');
        elsif rising_edge(clk) then
            if sample_load = '1' then
                sample_reg <= audio_samples(to_integer(unsigned(counter_value)));
            end if;
        end if;
    end process;

end rtl;
