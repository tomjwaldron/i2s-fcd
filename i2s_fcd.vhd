library ieee;
use ieee.std_logic_1164.all;

entity i2s_fcd is
    port (
        nrst : in std_logic;
        fast_clk : in std_logic;

        mclk, sdata : in std_logic;
        lrclk, bclk : out std_logic
    );
end i2s_fcd;

architecture rtl of i2s_fcd is

    constant i2s_data_width : positive := 24;
    
    signal audio_sample : std_logic_vector (i2s_data_width - 1 downto 0);
    signal audio_sample_left_ready : std_logic;
    signal audio_sample_right_ready : std_logic;
    signal audio_sample_ready : std_logic;

    signal audio_sample_fcd : std_logic_vector (i2s_data_width - 1 downto 0);
    signal audio_sample_fcd_ready : std_logic;

    signal ram_addr : std_logic_vector(0 downto 0) := (others => '0');
    signal ram_enabled : std_logic;
    signal ram_we : std_logic_vector(0 downto 0) := (others => '0');

    -- from vivado
    COMPONENT xilinx_dp_ram IS
    PORT (
      ENA        : IN STD_LOGIC;  --opt port
      WEA        : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
      ADDRA      : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
      DINA       : IN STD_LOGIC_VECTOR(23 DOWNTO 0);
      CLKA       : IN STD_LOGIC;
      ENB        : IN STD_LOGIC;  --opt port
      ADDRB      : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
      DOUTB      : OUT STD_LOGIC_VECTOR(23 DOWNTO 0);
      CLKB       : IN STD_LOGIC
    );
    END COMPONENT;

begin

    --------
    i2s_interface_inst: entity work.i2s_interface(rtl)
    generic map (BIT_WIDTH => i2s_data_width)
    port map (
        nrst => nrst,
        mclk => mclk,
        sdata => sdata,
        lrclk => lrclk,
        bclk => bclk,
        sample => audio_sample,
        sample_left_ready => audio_sample_left_ready,
        sample_right_ready => audio_sample_right_ready
    );

    --------
    handshake_inst: entity work.handshake(rtl)
    generic map (BIT_WIDTH => i2s_data_width)
    port map (
        nrst => nrst,
        talker_clk => mclk,
        listener_clk => fast_clk,
        data_in => audio_sample,
        data_in_ready => audio_sample_ready,
        data_out => audio_sample_fcd,
        data_out_ready => audio_sample_fcd_ready
    );

    audio_sample_ready <= '1' when audio_sample_left_ready
                                   or audio_sample_right_ready
                              else '0';

    ram_enabled <= not(nrst);

    -- -- from vivado's generator
    -- -- xilinx_dp_ram_inst: entity work.xilinx_dp_ram(xilinx_dp_ram_arch)
    -- xilinx_dp_ram_inst: xilinx_dp_ram
    -- port map (
    --     --Port A
    --     ENA        => ram_enabled,
    --     WEA        => ram_we,
    --     ADDRA      => ram_addr,
    --     DINA       => audio_sample_fcd,
    --     CLKA       => fast_clk,

    --     --Port B
    --     ENB        => '0', 
    --     ADDRB      => (others => '0'),
    --     -- DOUTB      => DOUTB,
    --     CLKB       => '0'
    -- );

end rtl;
