-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Sun Jul  7 21:22:53 2019
-- Host        : Tom-PC running 64-bit major release  (build 9200)
-- Command     : write_vhdl -mode funcsim -nolib -force -file
--               D:/Development/i2s-fcd/i2s-fcd-xilinx/i2s-fcd-xilinx.sim/sim_1/synth/func/xsim/i2s_fcd_testbench_func_synth.vhd
-- Design      : i2s_fcd
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z020clg484-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity clk_divider is
  port (
    bclk : out STD_LOGIC;
    mclk : in STD_LOGIC;
    AR : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end clk_divider;

architecture STRUCTURE of clk_divider is
  signal \^bclk\ : STD_LOGIC;
  signal clk_out_reg_i_1_n_0 : STD_LOGIC;
begin
  bclk <= \^bclk\;
clk_out_reg_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^bclk\,
      O => clk_out_reg_i_1_n_0
    );
clk_out_reg_reg: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => mclk,
      CE => '1',
      CLR => AR(0),
      D => clk_out_reg_i_1_n_0,
      Q => \^bclk\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \clk_divider__parameterized0\ is
  port (
    lrclk_OBUF : out STD_LOGIC;
    AR : out STD_LOGIC_VECTOR ( 0 to 0 );
    mclk : in STD_LOGIC;
    nrst_IBUF : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \clk_divider__parameterized0\ : entity is "clk_divider";
end \clk_divider__parameterized0\;

architecture STRUCTURE of \clk_divider__parameterized0\ is
  signal \^ar\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \clk_out_reg_i_1__0_n_0\ : STD_LOGIC;
  signal \count[7]_i_2_n_0\ : STD_LOGIC;
  signal count_next : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal count_reg : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \^lrclk_obuf\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \clk_out_reg_i_1__0\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \count[1]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \count[2]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \count[3]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \count[4]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \count[7]_i_1\ : label is "soft_lutpair1";
begin
  AR(0) <= \^ar\(0);
  lrclk_OBUF <= \^lrclk_obuf\;
\clk_out_reg_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF40"
    )
        port map (
      I0 => \count[7]_i_2_n_0\,
      I1 => count_reg(6),
      I2 => count_reg(7),
      I3 => \^lrclk_obuf\,
      O => \clk_out_reg_i_1__0_n_0\
    );
clk_out_reg_i_2: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => nrst_IBUF,
      O => \^ar\(0)
    );
clk_out_reg_reg: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => mclk,
      CE => '1',
      CLR => \^ar\(0),
      D => \clk_out_reg_i_1__0_n_0\,
      Q => \^lrclk_obuf\
    );
\count[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count_reg(0),
      O => count_next(0)
    );
\count[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => count_reg(0),
      I1 => count_reg(1),
      O => count_next(1)
    );
\count[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => count_reg(1),
      I1 => count_reg(0),
      I2 => count_reg(2),
      O => count_next(2)
    );
\count[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => count_reg(2),
      I1 => count_reg(0),
      I2 => count_reg(1),
      I3 => count_reg(3),
      O => count_next(3)
    );
\count[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => count_reg(3),
      I1 => count_reg(1),
      I2 => count_reg(0),
      I3 => count_reg(2),
      I4 => count_reg(4),
      O => count_next(4)
    );
\count[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => count_reg(4),
      I1 => count_reg(2),
      I2 => count_reg(0),
      I3 => count_reg(1),
      I4 => count_reg(3),
      I5 => count_reg(5),
      O => count_next(5)
    );
\count[6]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \count[7]_i_2_n_0\,
      I1 => count_reg(6),
      O => count_next(6)
    );
\count[7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D2"
    )
        port map (
      I0 => count_reg(6),
      I1 => \count[7]_i_2_n_0\,
      I2 => count_reg(7),
      O => count_next(7)
    );
\count[7]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFFFFFFFFFF"
    )
        port map (
      I0 => count_reg(4),
      I1 => count_reg(2),
      I2 => count_reg(0),
      I3 => count_reg(1),
      I4 => count_reg(3),
      I5 => count_reg(5),
      O => \count[7]_i_2_n_0\
    );
\count_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => mclk,
      CE => '1',
      CLR => \^ar\(0),
      D => count_next(0),
      Q => count_reg(0)
    );
\count_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => mclk,
      CE => '1',
      CLR => \^ar\(0),
      D => count_next(1),
      Q => count_reg(1)
    );
\count_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => mclk,
      CE => '1',
      CLR => \^ar\(0),
      D => count_next(2),
      Q => count_reg(2)
    );
\count_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => mclk,
      CE => '1',
      CLR => \^ar\(0),
      D => count_next(3),
      Q => count_reg(3)
    );
\count_reg[4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => mclk,
      CE => '1',
      CLR => \^ar\(0),
      D => count_next(4),
      Q => count_reg(4)
    );
\count_reg[5]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => mclk,
      CE => '1',
      CLR => \^ar\(0),
      D => count_next(5),
      Q => count_reg(5)
    );
\count_reg[6]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => mclk,
      CE => '1',
      CLR => \^ar\(0),
      D => count_next(6),
      Q => count_reg(6)
    );
\count_reg[7]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => mclk,
      CE => '1',
      CLR => \^ar\(0),
      D => count_next(7),
      Q => count_reg(7)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity i2s_interface is
  port (
    bclk : out STD_LOGIC;
    lrclk_OBUF : out STD_LOGIC;
    mclk : in STD_LOGIC;
    nrst_IBUF : in STD_LOGIC
  );
end i2s_interface;

architecture STRUCTURE of i2s_interface is
  signal \handshake_inst/four_phase_talker_inst/four_phase_talker_fsm_inst/l2_in\ : STD_LOGIC;
begin
bclk_divider_inst: entity work.clk_divider
     port map (
      AR(0) => \handshake_inst/four_phase_talker_inst/four_phase_talker_fsm_inst/l2_in\,
      bclk => bclk,
      mclk => mclk
    );
lrclk_divider_inst: entity work.\clk_divider__parameterized0\
     port map (
      AR(0) => \handshake_inst/four_phase_talker_inst/four_phase_talker_fsm_inst/l2_in\,
      lrclk_OBUF => lrclk_OBUF,
      mclk => mclk,
      nrst_IBUF => nrst_IBUF
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity i2s_fcd is
  port (
    nrst : in STD_LOGIC;
    fast_clk : in STD_LOGIC;
    mclk : in STD_LOGIC;
    sdata : in STD_LOGIC;
    lrclk : out STD_LOGIC;
    bclk : out STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of i2s_fcd : entity is true;
end i2s_fcd;

architecture STRUCTURE of i2s_fcd is
  signal bclk_OBUF : STD_LOGIC;
  signal lrclk_OBUF : STD_LOGIC;
  signal mclk_IBUF : STD_LOGIC;
  signal mclk_IBUF_BUFG : STD_LOGIC;
  signal nrst_IBUF : STD_LOGIC;
begin
bclk_OBUF_inst: unisim.vcomponents.OBUF
     port map (
      I => bclk_OBUF,
      O => bclk
    );
i2s_interface_inst: entity work.i2s_interface
     port map (
      bclk => bclk_OBUF,
      lrclk_OBUF => lrclk_OBUF,
      mclk => mclk_IBUF_BUFG,
      nrst_IBUF => nrst_IBUF
    );
lrclk_OBUF_inst: unisim.vcomponents.OBUF
     port map (
      I => lrclk_OBUF,
      O => lrclk
    );
mclk_IBUF_BUFG_inst: unisim.vcomponents.BUFG
     port map (
      I => mclk_IBUF,
      O => mclk_IBUF_BUFG
    );
mclk_IBUF_inst: unisim.vcomponents.IBUF
     port map (
      I => mclk,
      O => mclk_IBUF
    );
nrst_IBUF_inst: unisim.vcomponents.IBUF
     port map (
      I => nrst,
      O => nrst_IBUF
    );
end STRUCTURE;
