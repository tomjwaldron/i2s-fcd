library ieee;
use ieee.std_logic_1164.all;
use std.textio.all;

use work.lib.all;

entity clk_divider_tb is
end clk_divider_tb;

architecture behaviour of clk_divider_tb is
    constant test_run_time : time := 1 ms;
    constant test_reset_time : time := 50 ns;

    constant period : time := 1 sec / 24.576e6;
    constant bclk_divider : positive := 2;
    constant lrclk_divider : positive := 256;

    constant expected_bclk_period : time := 1 sec / 12.288e6;
    constant expected_lrclk_period : time := 1 sec / 96e3;

    signal nrst : std_logic := '0';
    signal tests_finished : std_logic := '0';

    signal audio_mclk : std_logic := '0';
    signal audio_bclk : std_logic := '0';
    signal audio_lrclk : std_logic := '0';
    
begin
    -------- 
    print_proc : process
        variable l : line;
    begin
        write (l, period);
        writeline (output, l);
        wait;
    end process print_proc;

    -------- 
    nrst <= '1' after test_reset_time;
    tests_finished <= '1' after test_run_time;

    --------
    audio_mclk <= not audio_mclk after period / 2
                  when tests_finished /= '1'
                  else '0';

    -------- 
    bclk_divider_inst: entity work.clk_divider (rtl)
    generic map
    (
        DIVIDER     => bclk_divider
    )
    port map
    (
        nrst        => nrst,
        clk_in      => audio_mclk,
        clk_out     => audio_bclk
    );

    -------- 
    lrclk_divider_inst: entity work.clk_divider (rtl)
    generic map
    (
        DIVIDER     => lrclk_divider
    )
    port map
    (
        nrst        => nrst,
        clk_in      => audio_mclk,
        clk_out     => audio_lrclk
    );

end behaviour;
