library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.lib.all;

entity clk_divider is
    generic (
        DIVIDER : positive
    );
    port (
        nrst : in std_logic;
        clk_in : in std_logic;
        clk_out : out std_logic
    );
end clk_divider;

architecture rtl of clk_divider is
    constant bit_width : positive := num_bits(DIVIDER);

    signal count, count_next : unsigned (bit_width - 1 downto 0) := (others => '0');

    signal clk_out_reg, clk_out_reg_en : std_logic := '0';
begin

    count_next <= count + 1;

    clk_out <= clk_out_reg;

    div_by_2_gen: if DIVIDER = 2 generate
        clk_out_reg_en <= '1';
    end generate;

    div_gen: if DIVIDER /= 2 generate
        clk_out_reg_en <= '1' when count = (DIVIDER - 1) else '0';
    end generate;

    count_register: process(nrst, clk_in)
    begin
        if not nrst then
            count <= (others => '0');
        elsif rising_edge(clk_in) then
            count <= count_next;
        end if;
    end process;

    clk_out_register: process(nrst, clk_in)
    begin
        if not nrst then
            clk_out_reg <= '0';
        elsif rising_edge(clk_in) then
            if clk_out_reg_en then
                clk_out_reg <= not clk_out_reg;
            end if;
        end if;
    end process;

end rtl;
