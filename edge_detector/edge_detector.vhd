library ieee;
use ieee.std_logic_1164.all;

entity edge_detector is
    generic (
        DETECT_RISING_EDGE : boolean := true
    );
    port (
        nrst, clk : in std_logic;
        strobe : in std_logic;
        detected : out std_logic
    );
end edge_detector;

architecture rtl of edge_detector is

    constant zero : std_logic_vector(1 downto 0) := "00";
    constant edge : std_logic_vector(1 downto 0) := "10";
    constant one : std_logic_vector(1 downto 0) := "01";
    
    signal state_reg, state_next : std_logic_vector(1 downto 0);
    signal high_low : std_logic;

begin

    direction_gen: if DETECT_RISING_EDGE generate
        high_low <= '1';
    else generate
        high_low <= '0';
    end generate;

    process(nrst, clk)
    begin
        if nrst = '0' then
            state_reg <= zero;
        elsif rising_edge(clk) then
            state_reg <= state_next;
        end if;
    end process;

    process(all)
    begin
        case state_reg is
            when zero =>
                if strobe = high_low then
                    state_next <= edge;
                else
                    state_next <= zero;
                end if;
            when edge =>
                if strobe = high_low then
                    state_next <= one;
                else
                    state_next <= zero;
                end if;
            when others =>
                if strobe = high_low then
                    state_next <= one;
                else
                    state_next <= zero;
                end if;
        end case;
    end process;

    detected <= state_reg(1);

end rtl;
