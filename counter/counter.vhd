library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity counter is
    generic (
        BIT_WIDTH : positive;
        INC : integer := 1;
        INIT : natural := 0
    );
    port (
        nrst : in std_logic;
        clk : in std_logic;
        enable : in std_logic := '1';
        reset : in std_logic := '0';
        count : out std_logic_vector(BIT_WIDTH - 1 downto 0)
    );
end counter;

architecture rtl of counter is

    signal count_reg, count_next : unsigned(BIT_WIDTH - 1 downto 0);

begin

    count_next <= count_reg + INC;

    count <= std_logic_vector (count_reg);

    process (nrst, clk)
    begin
        if nrst = '0' then
            count_reg <= to_unsigned (INIT, BIT_WIDTH);
        elsif rising_edge(clk) then
            if reset = '1' then
                count_reg <= to_unsigned (INIT, BIT_WIDTH);
            elsif enable = '1' then
                count_reg <= count_next;
            end if;
        end if;
    end process;

end rtl;
