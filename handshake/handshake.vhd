library ieee;
use ieee.std_logic_1164.all;

entity handshake is
    generic (BIT_WIDTH : positive);
    port (
        nrst : in std_logic;
        talker_clk : in std_logic;
        listener_clk : in std_logic;

        data_in : in std_logic_vector(BIT_WIDTH - 1 downto 0);
        data_in_ready : in std_logic;

        data_out : out std_logic_vector(BIT_WIDTH - 1 downto 0);
        data_out_ready : out std_logic;

        ready : out std_logic
    );
end handshake;

architecture rtl of handshake is

    signal req, ack, start : std_logic;

    signal data_in_reg : std_logic_vector(BIT_WIDTH - 1 downto 0);
    signal data_in_reg_en : std_logic;

    signal data_out_reg : std_logic_vector(BIT_WIDTH - 1 downto 0);
    signal data_out_reg_en : std_logic;

begin
    --------
    four_phase_talker_inst: entity work.four_phase_talker(rtl)
    port map (
        nrst => nrst, clk => talker_clk,
        ack_in => ack, start => start,
        req_out => req, ready => ready
    );

    --------
    four_phase_listener_inst: entity work.four_phase_listener(rtl)
    port map (
        nrst => nrst, clk => listener_clk,
        req_in => req, ack_out => ack,
        data_en => data_out_reg_en
    );

    --------
    process(nrst, talker_clk)
    begin
        if not nrst then
            start <= '0';
        elsif rising_edge(talker_clk) then
            if data_in_ready and ready then
                start <= '1';
            else
                start <= '0';
            end if;
        end if;
    end process;

    --------
    data_in_register: process(nrst, talker_clk)
    begin
        if not nrst then
            data_in_reg <= (others => '0');
        elsif rising_edge(talker_clk) then
            if data_in_ready then
                data_in_reg <= data_in;
            end if;
        end if;
    end process;

    --------
    data_out_register: process(nrst, listener_clk)
    begin
        if not nrst then
            data_out_reg <= (others => '0');
        elsif rising_edge(listener_clk) then
            if data_out_reg_en then
                data_out_reg <= data_in_reg;
            end if;
        end if;
    end process;

    --------
    data_out_ready_detect: entity work.edge_detector(rtl)
    port map (
        nrst => nrst, clk => listener_clk,
        strobe => data_out_reg_en,
        detected => data_out_ready
    );

    data_out <= data_out_reg;

end rtl;
