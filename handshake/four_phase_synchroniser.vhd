library ieee;
use ieee.std_logic_1164.all;

entity four_phase_synchroniser is
    port (
        nrst, clk : in std_logic;
        in_async : in std_logic;
        out_sync : out std_logic
    );
end four_phase_synchroniser;

architecture rtl of four_phase_synchroniser is

    signal meta_reg, meta_next : std_logic;
    signal sync_reg, sync_next : std_logic;

begin

    -- 2 registers
    process(nrst, clk)
    begin
        if not nrst then
            meta_reg <= '0';
            sync_reg <= '0';
        elsif rising_edge (clk) then
            meta_reg <= meta_next;
            sync_reg <= sync_next;
        end if;
    end process;

    -- next state logic
    meta_next <= in_async;
    sync_next <= meta_reg;

    -- output logic
    out_sync <= sync_reg;

end rtl;
