library ieee;
use ieee.std_logic_1164.all;

entity four_phase_listener_fsm is
    port (
        nrst, clk : in std_logic;
        req_sync : in std_logic;
        ack_out : out std_logic;
        data_en : out std_logic
    );
end four_phase_listener_fsm;

architecture rtl of four_phase_listener_fsm is

    type state_type is (ack0, ack1);

    signal state_reg, state_next : state_type;
    signal ack_buf_reg, ack_buf_next : std_logic;

begin

    state_register_and_output_buffer:
    process(nrst, clk)
    begin
        if not nrst then
            state_reg <= ack0;
            ack_buf_reg <= '0';
        elsif rising_edge(clk) then
            state_reg <= state_next;
            ack_buf_reg <= ack_buf_next;
        end if;
    end process;

    next_state_logic:
    process(all)
    begin
        state_next <= state_reg;
        data_en <= '0';
        case state_reg is
            when ack0 =>
                if req_sync then
                    state_next <= ack1;
                    data_en <= '1';
                end if;
            when ack1 =>
                if not req_sync then
                    state_next <= ack0;
                end if;
        end case;
    end process;

    look_ahead_output_logic:
    process(all)
    begin
        case state_next is
            when ack0 =>
                ack_buf_next <= '0';
            when ack1 =>
                ack_buf_next <= '1';
        end case;
    end process;

    ack_out <= ack_buf_reg;

end rtl;

--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity four_phase_listener is
    port (
        nrst, clk : in std_logic;
        req_in : in std_logic;
        ack_out : out std_logic;
        data_en : out std_logic
    );
end four_phase_listener;

architecture rtl of four_phase_listener is

    signal req_sync : std_logic;

begin

    four_phase_synchroniser_inst: entity work.four_phase_synchroniser (rtl)
    port map (
        nrst => nrst, clk => clk,
        in_async => req_in,
        out_sync => req_sync
    );

    four_phase_listener_fsm_inst: entity work.four_phase_listener_fsm (rtl)
    port map (
        nrst => nrst, clk => clk, 
        req_sync => req_sync,
        ack_out => ack_out,
        data_en => data_en
    );

end rtl;
