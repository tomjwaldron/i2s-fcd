library ieee;
use ieee.std_logic_1164.all;

entity four_phase_talker_fsm is
    port (
        nrst, clk : in std_logic;
        start, ack_sync : in std_logic;
        ready, req_out : out std_logic
    );
end four_phase_talker_fsm;

architecture rtl of four_phase_talker_fsm is

    type state_type is (idle, req1, req0);

    signal state_reg, state_next : state_type;
    signal req_buf_reg, req_buf_next : std_logic;

begin

    state_register_and_output_buffer:
    process(nrst, clk)
    begin
        if not nrst then
            state_reg <= idle;
            req_buf_reg <= '0';
        elsif rising_edge(clk) then
            state_reg <= state_next;
            req_buf_reg <= req_buf_next;
        end if;
    end process;

    next_state_logic:
    process(all)
    begin
        ready <= '0';
        state_next <= state_reg;
        case state_reg is
            when idle =>
                if start then
                    state_next <= req1;
                end if;
                ready <= '1';
            when req1 =>
                if ack_sync then
                    state_next <= req0;
                end if;
            when req0 =>
                if not ack_sync then
                    state_next <= idle;
                end if;
        end case;
    end process;

    look_ahead_output_logic:
    process(all)
    begin
        case state_next is
            when idle =>
                req_buf_next <= '0';
            when req1 =>
                req_buf_next <= '1';
            when req0 =>
                req_buf_next <= '0';
        end case;
    end process;

    req_out <= req_buf_reg;

end rtl;

--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity four_phase_talker is
    port (
        nrst, clk : in std_logic;
        ack_in : in std_logic;
        start : in std_logic;
        ready : out std_logic;
        req_out : out std_logic
    );
end four_phase_talker;

architecture rtl of four_phase_talker is

    signal ack_sync : std_logic;

begin

    four_phase_synchroniser_inst: entity work.four_phase_synchroniser(rtl)
    port map (
        nrst => nrst, clk => clk,
        in_async => ack_in,
        out_sync => ack_sync
    );

    four_phase_talker_fsm_inst: entity work.four_phase_talker_fsm(rtl)
    port map (
        nrst => nrst, clk => clk, 
        start => start, ack_sync => ack_sync,
        ready => ready, req_out => req_out
    );

end rtl;
