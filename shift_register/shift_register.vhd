library ieee;
use ieee.std_logic_1164.all;

entity shift_register is
    generic (
        BIT_WIDTH : positive;
        LSB_FIRST : boolean := false
    );
    port (
        clk, nrst : in std_logic;

        shift : in std_logic := '1';
        serial_out : out std_logic;
        serial_in : in std_logic := '0';

        parallel_load : in std_logic := '0';
        parallel_out : out std_logic_vector (BIT_WIDTH - 1 downto 0);
        parallel_in : in std_logic_vector (BIT_WIDTH - 1 downto 0) := (others => '0')
    );
end shift_register;

architecture rtl of shift_register is
    
    constant top_bit : positive := BIT_WIDTH - 1;

    signal reg, reg_next : std_logic_vector (top_bit downto 0);

begin

    process (nrst, clk)
    begin
        if nrst = '0' then
            reg <= (others => '0');
        elsif rising_edge (clk) then
            if parallel_load = '1' then
                reg <= parallel_in;
            elsif shift = '1' then
                reg <= reg_next;
            else
                reg <= reg;
            end if;
        end if;
    end process;

    parallel_out <= reg;

    msb_lsb_gen: if LSB_FIRST generate
        reg_next <= serial_in & reg (top_bit downto 1);     -- right shift
        serial_out <= reg (0);
    else generate
        reg_next <= reg (top_bit - 1 downto 0) & serial_in; -- left shift
        serial_out <= reg (top_bit);
    end generate;

end architecture;
