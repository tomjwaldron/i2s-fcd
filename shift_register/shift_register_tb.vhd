library ieee;
use ieee.std_logic_1164.all;
use std.textio.all;

use work.lib.all;

entity shift_register_tb is
end shift_register_tb;

architecture behaviour of shift_register_tb is
    constant test_run_time : time := 1 ms;
    constant test_reset_time : time := 50 ns;

    constant period : time := 1 sec / 24.576e6;

    signal nrst : std_logic := '0';
    signal tests_finished : std_logic := '0';

    signal clock : std_logic := '0';
    
begin
    -------- 
    nrst <= '1' after test_reset_time;
    tests_finished <= '1' after test_run_time;

    --------
    clock <= not clock after period / 2
             when tests_finished /= '1'
             else '0';

    -------- 
    shift_register_inst: entity work.shift_register (rtl)
    generic map
    (
        BIT_WIDTH       => 24
    )
    port map
    (
        clk             => clock,
        nrst            => nrst,
        serial_in       => input_data,
        parallel_out    => output_data
    );

    --------
    test_sequence: process
    begin
        wait for nrst;
        
    end process test_sequence;

end behaviour;
