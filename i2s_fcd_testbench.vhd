library ieee;
use ieee.std_logic_1164.all;
use std.textio.all;
use work.lib.all;

entity i2s_fcd_testbench is
end i2s_fcd_testbench;

architecture behaviour of i2s_fcd_testbench is

    constant test_run_time : time := 10 ms;
    constant test_reset_time : time := 50 ns;

    constant fast_clk_period : time := 1 sec / 256e6;
    constant mclk_period : time := 1 sec / 24.576e6;

    constant i2s_data_width : positive := 24;

    signal nrst : std_logic := '0';
    signal tests_finished : std_logic := '0';

    signal fast_clk : std_logic := '0';
    
    signal audio_mclk, audio_sdata : std_logic := '0';
    signal audio_bclk, audio_lrclk : std_logic;

begin
    -------- 
    nrst <= '1' after test_reset_time;
    tests_finished <= '1' after test_run_time;

    --------
    fast_clk_stim:
    fast_clk <= not fast_clk after fast_clk_period / 2
                when tests_finished = '0'
                else '0';

    --------
    audio_mclk_stim:
    audio_mclk <= not audio_mclk after mclk_period / 2
                  when tests_finished = '0'
                  else '0';

    --------
    adc_inst: entity work.adc (rtl)
    port map (
        nrst => nrst,
        clk => audio_mclk,
        sdata => audio_sdata,
        lrclk => audio_lrclk,
        bclk => audio_bclk
    );

    --------
    -- i2s_fcd_inst: entity work.i2s_fcd (rtl)
    i2s_fcd_inst: entity work.i2s_fcd (STRUCTURE) -- NOTE: vivado sim bug: https://forums.xilinx.com/t5/Simulation-and-Verification/Post-Synth-simulation-cant-find-architecture/td-p/836589
    port map (
        nrst => nrst,
        fast_clk => fast_clk,
        mclk => audio_mclk,
        sdata => audio_sdata,
        lrclk => audio_lrclk,
        bclk => audio_bclk
    );

end behaviour;
