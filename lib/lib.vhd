library ieee;
use ieee.std_logic_1164.all;
use ieee.math_real.all;
use ieee.numeric_std.all;

package lib is
    -- type positive_vector is array (natural range <>) of positive;
    -- type samp_vec is array (natural range <>) of std_logic_vector;
    -- type samp_vec_signed is array (natural range <>) of signed;
    -- type samp_vec_int is array (natural range <>) of integer;

    function num_bits(val : integer) return natural;
    function to_bool(val : std_logic) return boolean;
    function to_logic(val : boolean) return std_logic;
end lib;

package body lib is
    function num_bits(val: integer) return natural is
    begin
        return integer(ceil(log2(real(val))));
    end;

    function to_bool(val : std_logic) return boolean is
    begin
        if val = '1' then
            return true;
        else
            return false;
        end if;
    end;

    function to_logic(val : boolean) return std_logic is
    begin
        if val then
            return '1';
        else
            return '0';
        end if;
    end;
end lib;
