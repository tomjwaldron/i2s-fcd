library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.lib.all;

entity i2s_interface is
    generic (
        BIT_WIDTH : positive
    );
    port (
        nrst : in std_logic;
        mclk : in std_logic;
        sdata : in std_logic;
        lrclk, bclk : out std_logic;

        sample : out std_logic_vector (BIT_WIDTH - 1 downto 0);
        sample_left_ready : out std_logic;
        sample_right_ready : out std_logic
    );
end i2s_interface;

architecture rtl of i2s_interface is

    constant bclk_divider : positive := 2;
    constant lrclk_divider : positive := 256;
    constant counter_width : natural := num_bits(BIT_WIDTH);

    signal audio_sample_reg : std_logic_vector(BIT_WIDTH - 1 downto 0);
    signal left_ready_reg, right_ready_reg : std_logic;

    signal counter_value : std_logic_vector(counter_width - 1 downto 0);
    signal counter_reset : std_logic;

    signal bit_count_reached : std_logic;

    signal lrclk_rising, lrclk_falling : std_logic;
    signal lrclk_reg : std_logic;

begin

    -------- 
    bclk_divider_inst: entity work.clk_divider (rtl)
    generic map (DIVIDER => bclk_divider)
    port map (nrst => nrst, clk_in => mclk, clk_out => bclk);

    -------- 
    lrclk_divider_inst: entity work.clk_divider (rtl)
    generic map (DIVIDER => lrclk_divider)
    port map (nrst => nrst, clk_in => mclk, clk_out => lrclk);

    --------
    rising_lrclk_inst: entity work.edge_detector(rtl)
    port map (
        nrst => nrst, clk => mclk,
        strobe => lrclk,
        detected => lrclk_rising
    );

    --------
    falling_lrclk_inst: entity work.edge_detector(rtl)
    generic map ( DETECT_RISING_EDGE => false )
    port map (
        nrst => nrst, clk => mclk,
        strobe => lrclk,
        detected => lrclk_falling
    );

    --------
    counter_inst: entity work.counter(rtl)
    generic map(BIT_WIDTH => counter_width)
    port map (
        nrst => nrst, clk => mclk,
        enable => bclk,
        reset => counter_reset,
        count => counter_value
    );

    -------- 
    shift_register_inst: entity work.shift_register (rtl)
    generic map (BIT_WIDTH => BIT_WIDTH)
    port map (clk => mclk, nrst => nrst,
              shift => bclk,
              serial_in => sdata,
              parallel_out => audio_sample_reg);

    ready_register: process(nrst, mclk)
    begin
        if not nrst then
            left_ready_reg <= '0';
            right_ready_reg <= '0';
        elsif rising_edge(mclk) then
            if bit_count_reached and lrclk then
                right_ready_reg <= '1';
            elsif bit_count_reached and not lrclk then
                left_ready_reg <= '1';
            else
                right_ready_reg <= '0';
                left_ready_reg <= '0';
            end if;
        end if;
    end process;

    --------
    bit_count_reached_register: process(nrst, mclk) is
    begin
        if not nrst then
            bit_count_reached <= '0';
        elsif rising_edge(mclk) then
            if unsigned(counter_value) = BIT_WIDTH - 1 then
                bit_count_reached <= '1';
            else
                bit_count_reached <= '0';
            end if;
        end if;
    end process;

    --------
    counter_reset_register: process(nrst, mclk) is
    begin
        if not nrst then
            counter_reset <= '0';
        elsif rising_edge(mclk) then
            if bit_count_reached then
                counter_reset <= '1';
            elsif lrclk_reg then
                counter_reset <= '0';
            else
                counter_reset <= counter_reset;
            end if;
        end if;
    end process;

    --------
    lrclk_reg <= '1' when lrclk_rising or lrclk_falling else '0';

    -- output logic
    sample_left_ready <= left_ready_reg;
    sample_right_ready <= right_ready_reg;
    sample <= audio_sample_reg;

end rtl;
